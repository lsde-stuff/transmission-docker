FROM alpine
RUN apk add --no-cache transmission-daemon
VOLUME /data
VOLUME /config
USER nobody
WORKDIR /tmp
EXPOSE 5000/tcp
EXPOSE 51413/udp
CMD ["transmission-daemon", "-f", "-w", "/data", "-g", "/config", "-p", "5000", "--no-incomplete-dir"]
